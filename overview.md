# A pretty snazzy lunch

This is a simple example of how to use [GitBuilding](https://gitbuilding.io). Rather than an open hardware project, this documents "building" a lunch which is, *I feel*, pretty snazzy. You can view the [source for this documentation on GitLab](https://gitlab.com/gitbuilding/gitbuilding-example).

There are two different (yet similar) variations of the lunch:

* The **[original lunch](original_lunch.md)** pretty snazzy lunch
* A version with a **[modified lunch](modified_lunch.md)** with a cheeseboard requested by William.
